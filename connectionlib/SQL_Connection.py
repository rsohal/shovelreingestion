import pymysql
import sshtunnel
import json
configs = json.loads(open('config/connection.json').read())


def getConnection(connection_name):
    credentials = configs[connection_name]
    bridge = configs[credentials['bridge']]
    print credentials,bridge
    tunnel = sshtunnel.SSHTunnelForwarder(bridge['host'], ssh_username=bridge['user'],ssh_private_key=bridge['key'], ssh_password=bridge['password'],
                                          remote_bind_address=(credentials['host'], credentials['port']))
    tunnel.start()
    port = tunnel.local_bind_port
    connection = pymysql.connect(user=credentials['user'], passwd=credentials['password'], host='127.0.0.1', port=port, database=credentials['db'])
    return connection


