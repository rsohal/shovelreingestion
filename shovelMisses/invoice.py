import logging
import  sys
from connectionlib.SQL_Connection import getConnection
from connectionlib.Hive_Connection import hiveConnection
from datetime import date, timedelta, datetime
from scripts.helper import convertCase
from scripts.helper import writeResult
from scripts.helper import getPartitonKey
from scripts.helper import getDatefromDelta


FILE_PATH = "output/shovel/new/"
TYPE = ["PayableCreditNote","PayableDebitNote"]
APLQuery = "select id from core_invoices where type = 'INVOICE_TYPE' and partition_key in (PARTITION_KEY)"
HIVEQuery = "select id from wallstreet.core_invoices_rtd_BU_YESTERDAY where type = 'INVOICE_TYPE' and created_at_ts >= START and created_at_ts < TODAY"
ResultQuery = "select id,type from core_invoices where id in (MISSED_ID)"
YESTERDAY = (date.today() - timedelta(1)).strftime('%Y%m%d')


def getAplIDs(bu,type,startDaydelta,endDaydelta):
    rows = []
    try:
        connection = getConnection("core_invoice_"+bu)
        aplCursor = connection.cursor()
        query = APLQuery.replace("PARTITION_KEY",",".join(getPartitonKey(startDaydelta,endDaydelta))).replace("INVOICE_TYPE",type)
        logging.info(query)
        try:
            aplCursor.execute(query)
            result = aplCursor.fetchall()
            for r in result:
                rows.append(r[0])
        except:
            logging.error("Error in running query:",sys.exc_info()[0])
            pass
        aplCursor.close()
        connection.close()
    except:
        logging.error("Could not get "+bu+" Invoice Connection: ")
        pass
    return rows

def getHiveIds(bu,type,startDaydelta,endDaydelta):
    rows = []
    try:
        connection = hiveConnection()
        hiveCursor = connection.cursor()
        a = getDatefromDelta(endDaydelta)
        print a, startDaydelta,endDaydelta
        query = HIVEQuery.replace("BU",bu).replace("YESTERDAY",YESTERDAY).replace("TODAY",getDatefromDelta(endDaydelta)).replace("START",getDatefromDelta(startDaydelta)).replace("INVOICE_TYPE",convertCase(type))
        logging.info(query)
        try:
            hiveCursor.execute(query)
            result = hiveCursor.fetchall()
            for r in result:
                rows.append(r[0])
        except:
            logging.error("Error in running Hive Query")
            pass
        hiveCursor.close()
        connection.close()
    except:
        logging.error("Could not get "+bu+" Invoice HIVE Connection: ", sys.exc_info()[0])
        pass
    return rows


def queryDataToIngest(bu,type,startDaydelta,endDaydelta):
    rows = []
    apl = set(getAplIDs(bu,type,startDaydelta, endDaydelta))
    hive = set(getHiveIds(bu,type,startDaydelta, endDaydelta))
    diff = list(apl - hive)
    writeResult("output/IngestionData/"+bu+"_"+type+"_"+getDatefromDelta(startDaydelta)+"TO"+getDatefromDelta(endDaydelta),diff)
    if len(apl) > 0 and len(hive) > 0 and len(diff) > 0:
        try:
            connection = getConnection("core_invoice_"+bu)
            aplCursor = connection.cursor()
            chunks = [diff[x:x + 25000] for x in xrange(0, len(diff), 25000)]
            for d in chunks:
                query = ResultQuery.replace("MISSED_ID",','.join(map("'{0}'".format, d)))
                logging.info("Ingesting %s records of invoices",len(d))
                aplCursor.execute(query)
                result = aplCursor.fetchall()
                for r in result:
                    rows.append("""('invoice_by_id','{"id":"""+'"'+r[0]+"""","type":"""+'"'+convertCase(r[1])+""""}','""" + "shovelInvoiceManual"+ datetime.now().strftime('%Y%m%d%H%M') + """','{"X_BU_ID":"myntramp","X_INVOICE_TYPE":"""+'"'+convertCase(r[1])+'"'+"""}','created',None,datetime.now().strftime('%Y-%m-%d %H:%M:%S'), datetime.now().strftime('%Y-%m-%d %H:%M:%S'), datetime.now().strftime('%Y-%m-%d %H:%M:%S') ,  None,"""+ date.today().strftime('%Y%m%d')+')')
            aplCursor.close()
            connection.close()
        except:
            logging.error("Could not get" +bu+ "Invoice Connection: ", sys.exc_info()[0])
            pass
    elif len(apl) == 0:
        logging.error(bu+type +" Invoice Apl Query Returning 0")
    elif len(hive) == 0:
        logging.error(bu+type +" Invoice Hive Query Returning 0")
    else:
        logging.error( bu+type +" Invoice { Apl_count :"+str(len(apl))+", Hive_count:"+str(len(hive))+", diff:" + str(len(diff)))
    return rows




def generateShovelDataforInvoice(bu,start,end):
    startDaydelta = 3
    endDaydelta = 0
    if start is not None and end is not None:
        startDaydelta = abs(((datetime.now()) - (datetime.strptime(start,"%Y-%m-%d"))).days)
        endDaydelta = abs(((datetime.now()) - (datetime.strptime(end,"%Y-%m-%d"))).days)
    for t in TYPE:
        path = FILE_PATH + datetime.now().strftime('%Y%m%d%H%M') + '.txt'
        result= queryDataToIngest(bu,t,startDaydelta,endDaydelta)
        if len(result) > 0:
            writeResult(path,result)
            logging.info(bu + t +" Invoice Diffs been written successfully to "+path)
        else:
            logging.info("No Shovel Diffs Found for "+bu+" Invoices")



