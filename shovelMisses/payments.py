from connectionlib.SQL_Connection import getConnection
from connectionlib.Hive_Connection import hiveConnection
from datetime import date, timedelta, datetime
from scripts.helper import convertCase
from scripts.helper import writeResult
from scripts.helper import getPartitonKey
from scripts.helper import getDatefromDelta
import logging

FILE_PATH = "../output/shovel/new/"
YESTERDAY =  (date.today()- timedelta(1)).strftime('%Y%m%d')
APLQuery = "select client_ref_id from core_payments where partition_key in (PARTITION_KEY)"
HIVEQuery = "select client_ref_id from wallstreet.payments_rtd_YESTERDAY where  created_at_ts >= START and created_at_ts < TODAY"
resultQuery = "select client_ref_id,type from core_payments where id in (MISSED_ID)"


def getAplIDs(startDaydelta,endDaydelta):
    rows = []
    try:
        connection = getConnection("core_payments")
        aplCursor = connection.cursor()
        query = APLQuery.replace("PARTITION_KEY", ",".join(getPartitonKey(startDaydelta,endDaydelta)))
        logging.info(query)
        try:
            aplCursor.execute(query)
            result = aplCursor.fetchall()
            for r in result:
                rows.append(r[0])
            aplCursor.close()
            connection.close()
        except:
            logging.error("Error in  running payments apl query:")
            pass
    except:
        logging.error("Could not get Payments Connection: ")
        pass
    return rows

def getHiveIds(startDaydelta,endDaydelta):
    rows = []
    try:
        connection = hiveConnection()
        hiveCursor = connection.cursor()
        query = HIVEQuery.replace("YESTERDAY", YESTERDAY).replace("TODAY", getDatefromDelta(endDaydelta)).replace("START", getDatefromDelta(startDaydelta))
        logging.info(query)
        try:
            hiveCursor.execute(query)
            result = hiveCursor.fetchall()
            for r in result:
                rows.append(r[0])
            hiveCursor.close()
            connection.close()
        except:
            logging.error("Error in running payments hive query:")
            pass
    except:
        logging.error("Could not get Payments hive Query Connection: ")
        raise
    return rows


def queryDataToIngest(startDaydelta,endDaydelta):
    rows = []
    apl = set(getAplIDs(startDaydelta, endDaydelta))
    hive = set(getHiveIds(startDaydelta, endDaydelta))
    diff = list(apl - hive)
    writeResult("output/IngestionData/Payments_" + getDatefromDelta(startDaydelta) + "TO" + getDatefromDelta(endDaydelta),diff)
    if len(apl) > 0 and len(hive) > 0 and len(diff) > 0:
        try:
            connection = getConnection("core_payments")
            aplCursor = connection.cursor()
            query = resultQuery.replace("MISSED_ID",','.join(map("'{0}'".format, diff)))
            print query
            aplCursor.execute(query)
            result = aplCursor.fetchall()
            for r in result:
                rows.append("""('payment','{"clientRefId":"""+'"'+r[0]+"""","type":"""+'"'+convertCase(r[1])+""""}','""" + "shovelPaymentsManual"+ datetime.now().strftime('%Y%m%d%H%M') + """','{"X_BU_ID":"myntramp"""+'"'+"""}','created',None,datetime.now().strftime('%Y-%m-%d %H:%M:%S'), datetime.now().strftime('%Y-%m-%d %H:%M:%S'), datetime.now().strftime('%Y-%m-%d %H:%M:%S') ,  None,"""+ date.today().strftime('%Y%m%d')+')')
        except:
            logging.error("Could not get APL Application Connection: ")
            pass
    elif len(apl ) == 0:
        logging.error("Payments Apl Query Returning 0")
    elif len(hive ) == 0:
        logging.error("Payments Hive Query Returning 0")
    else:
        logging.error("Payments { Apl_count :"+str(len(apl))+", Hive_count:"+str(len(hive))+", diff:" + str(len(diff)))
    return rows

def generateShovelDataPayments(start,end):
    startDaydelta = 3
    endDaydelta = 0
    if start is not None and end is not None:
        startDaydelta = abs(((datetime.now()) - (datetime.strptime(start,"%Y-%m-%d"))).days)
        endDaydelta = abs(((datetime.now()) - (datetime.strptime(end,"%Y-%m-%d"))).days)
    path = FILE_PATH + datetime.now().strftime('%Y%m%d%H%M') + '.txt'
    result= queryDataToIngest(startDaydelta,endDaydelta)
    if len(result) > 0:
        writeResult(path,result)
        print "Payments Shovel Diffs been written successfully to ",path
    else:
        print "no gaps found for Payments Shovel"




