from connectionlib.SQL_Connection import getConnection
import logging
from connectionlib.Hive_Connection import hiveConnection
from datetime import date, timedelta, datetime
from scripts.helper import writeResult
from scripts.helper import getPartitonKey
from scripts.helper import getDatefromDelta


FILE_PATH= "output/shovel/new/"
YESTERDAY=  (date.today()- timedelta(2)).strftime('%Y%m%d')
APLQuery= "select id from core_invoice_payments where partition_key in (PARTITION_KEY)"
HIVEQuery= "select id from wallstreet.application_rtd_YESTERDAY where created_at_ts >= START and created_at_ts < TODAY"
ResultQuery= "select client_ref_id from core_invoice_payments where id in (MISSED_ID)"


def getAplIDs(startDaydelta,endDaydelta):
    rows = []
    try:
        connection = getConnection("application")
        aplCursor = connection.cursor()
        query = APLQuery.replace("PARTITION_KEY",",".join(getPartitonKey(startDaydelta,endDaydelta)))
        logging.info(query)
        try:
            aplCursor.execute(query)
            result = aplCursor.fetchall()
            for r in result:
                rows.append(r[0])
            aplCursor.close()
            connection.close()
        except:
            logging.error("Error in  running application apl query:")
            pass
    except:
        logging.error("Could not get MP Invoice Connection: ")
        pass
    return rows

def getHiveIds(startDaydelta,endDaydelta):
    rows = []
    try:    
        connection = hiveConnection()
        hiveCursor = connection.cursor()
        query = HIVEQuery.replace("YESTERDAY",YESTERDAY).replace("TODAY",getDatefromDelta(endDaydelta)).replace("START",getDatefromDelta(startDaydelta))
        logging.info(query)
        try:
            hiveCursor.execute(query)
            result = hiveCursor.fetchall()
            for r in result:
                rows.append(r[0])
            hiveCursor.close()
            connection.close()
        except:
            logging.error("Error in running application hive query:")
            pass
    except:
        logging.error("Could not get Application HIVEQuery Connection: ")
        pass
    return rows


def queryDataToIngest(startDaydelta,endDaydelta):
    rows = []
    apl =  set(getAplIDs(startDaydelta,endDaydelta))
    hive = set(getHiveIds(startDaydelta,endDaydelta))
    diff = list(apl - hive)
    writeResult("output/IngestionData/Payments_" + getDatefromDelta(startDaydelta) + "TO" + getDatefromDelta(endDaydelta),diff)
    if len(apl) > 0 and len(hive) > 0 and len(diff) > 0:
        try:
            connection = getConnection("application")
            aplCursor = connection.cursor()
            chunks = [diff[x:x + 25000] for x in xrange(0, len(diff), 25000)]
            for d in chunks:
                query = ResultQuery.replace("MISSED_ID",','.join(map("'{0}'".format, d)))
                logging.info("Ingesting %s records of application",len(d))
                aplCursor.execute(query)
                result = aplCursor.fetchall()
                for r in result:
                    rows.append("""('i2p_application','{"clientRefId":"""+'"'+r[0]+"""","type":"i2p_application"""+""""}','""" + "shovelApplicationManual"+ datetime.now().strftime('%Y%m%d%H%M') + """','{"X_BU_ID":"myntramp"""+'"'+"""}','created',None,datetime.now().strftime('%Y-%m-%d %H:%M:%S'), datetime.now().strftime('%Y-%m-%d %H:%M:%S'), datetime.now().strftime('%Y-%m-%d %H:%M:%S') ,  None,"""+ date.today().strftime('%Y%m%d')+')')
            aplCursor.close()
            connection.close()
        except:
            logging.error("Could not get APL Application Connection: ")
            pass
    elif len(apl ) == 0:
        logging.error("Application Apl Query Returning 0")
    elif len(hive ) == 0:
        logging.error("Application Hive Query Returning 0")
    else:
        logging.error("Application { Apl_count :"+str(len(apl))+", Hive_count:"+str(len(hive))+", diff:" + str(len(diff)))
    return rows



def generateShovelDataApplication(start,end):
    startDaydelta = 3
    endDaydelta = 0
    if start is not None and end is not None:
        startDaydelta = abs(((datetime.now()) - (datetime.strptime(start,"%Y-%m-%d"))).days)
        endDaydelta = abs(((datetime.now()) - (datetime.strptime(end,"%Y-%m-%d"))).days)
    path = FILE_PATH + datetime.now().strftime('%Y%m%d%H%M') + '.txt'
    result= queryDataToIngest(startDaydelta,endDaydelta)
    if len(result) > 0:
        writeResult(path, result)
        print "Application Shovel Diffs been written successfully to ", path
    else:
        print "no gaps found for Application Shovel"




