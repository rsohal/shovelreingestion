from connectionlib.SQL_Connection import getConnection
import logging
import sys
from scripts.helper import getQuery
from datetime import date, timedelta,datetime
from scripts.helper import writeResult
from scripts.helper import getSCMIngestPayload
from scripts.helper import connectionName
from scripts.helper import getEvents
from scripts.helper import getBus

BU = "mp"
FILE_PATH = 'output/source/new/'
PARTITION_KEY = (date.today()- timedelta(2)).strftime('%Y%m%d')
START_DATE = (date.today()- timedelta(2)).strftime('%Y-%m-%d')
END_DATE = date.today().strftime('%Y-%m-%d')

def getSourceData(bu,event):
    rows = []
    try:
        connection = getConnection(connectionName(bu,"scm",event))
        omsCursor = connection.cursor()
        omsQuery = getQuery(bu,"scm",event)
        query = omsQuery.replace("START_DATE",START_DATE).replace("END_DATE",END_DATE)
        logging.info(query)
        try:
            omsCursor.execute(query)
            result = omsCursor.fetchall()
            for r in result:
                rows.append(r[0])
        except:
            logging.error("Error in running query:", sys.exc_info()[0])
            pass
    except:
        logging.error("Could not get OMS Connection")
        pass
    logging.info("OMS Count:"+str(len(rows)))
    return rows


def getAPlData(bu,event):
    rows = []
    try:
        connection = getConnection(connectionName(bu,"apl",event))
        aplCursor = connection.cursor()
        aplQuery = getQuery(bu, "apl", event)
        query = aplQuery.replace("START_DATE", START_DATE).replace("END_DATE", END_DATE).replace("PARTITION_KEY", PARTITION_KEY)
        logging.info(query)
        try:
            aplCursor.execute(query)
            result = aplCursor.fetchall()
            for r in result:
                rows.append(r[0])
        except:
            logging.error("Error in running query:", sys.exc_info()[0])
            pass
    except:
        logging.error("Could not get OMS Connection")
        pass
    logging.info("APL Count:" + str(len(rows)))
    return rows

def queryDataToIngest(bu,event):
    rows = []
    oms = [str(i) for i in getSourceData(bu,event)]
    diff = list(set(oms) - set(getAPlData(bu,event)))
    if len(diff) > 0:
        try:
            connection = getConnection(connectionName(bu,'ingest',event))
            omsCursor = connection.cursor()
            chunks = [diff[x:x + 25000] for x in xrange(0, len(diff), 25000)]
            for d in chunks:
                omsQuery = getQuery(bu,"ingest",event)
                query = omsQuery.replace("START_DATE",START_DATE).replace("END_DATE",END_DATE).replace("MISSED_ID",','.join(map("'{0}'".format, d)))
                logging.info(len(chunks))
                omsCursor.execute(query)
                result = omsCursor.fetchall()
                rows = getSCMIngestPayload(result,bu,event)
        except:
            logging.error("Could not get OMS Connection")
            pass
            
    return rows

def getMissingData():
    BUs = getBus()
    for bu in BUs:
        events = getEvents(bu)
        for event in events:
            path = FILE_PATH +bu +"_"+ event + "_" + datetime.now().strftime('%Y%m%d%H%M') + '.txt'
            result = queryDataToIngest(BU,event)
            if len(result) > 0:
                writeResult(path, result)
                logging.info("Diffs been written successfully to " + path)
            else:
                logging.info("No Source Diffs Found for"+bu+event)
    return