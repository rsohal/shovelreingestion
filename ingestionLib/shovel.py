import shutil
import logging
from os import listdir
from os.path import isfile, join
from connectionlib.SQL_Connection import getConnection
from datetime import datetime
NEW_FILE_PATH="output/shovel/new/"
PROCESSED_FILE_PATH="output/shovel/processed"
SHOVEL_INSERT="insert into `events` (`entity_name`, `context`, `group_id`, `headers`, `status`, `processed_at`, `created_at`, `updated_at`, `last_picked_at`, `ack_info`, `partition_key`) VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"

def newFilestoPush():
    return [f for f in listdir(NEW_FILE_PATH) if isfile(join(NEW_FILE_PATH, f))]

def moveToupdated():
    files = newFilestoPush()
    for file in files:
        logging.info("moving "+NEW_FILE_PATH+file+" to "+PROCESSED_FILE_PATH)
        shutil.move(NEW_FILE_PATH+file, PROCESSED_FILE_PATH)

def getRows():
    rows = []
    files = newFilestoPush()
    for file in files:
        with open(NEW_FILE_PATH+file) as f:
            for line in f.readlines():
                line = line.strip('\n')
                rows.append(eval(line))
    return rows


def ingest():
    rows = getRows()
    connection = getConnection("shovel")
    shovelCursor = connection.cursor()
    shovelCursor.executemany(SHOVEL_INSERT, rows)
    connection.commit()
    moveToupdated()
    logging.info(str(shovelCursor.rowcount) + "rows inserted successfully.")







