import shutil
import logging
from os import listdir
from os.path import isfile, join
from scripts.push_to_asl import pushtoAsl
from datetime import datetime


NEW_FILE_PATH="output/source/new/"
PROCESSED_FILE_PATH="output/source/processed"

def newFilestoPush():
    return [f for f in listdir(NEW_FILE_PATH) if isfile(join(NEW_FILE_PATH, f))]

def moveToupdated():
    files = newFilestoPush()
    for file in files:
        logging.info("moving "+NEW_FILE_PATH+file+" to "+PROCESSED_FILE_PATH)
        shutil.move(NEW_FILE_PATH+file, PROCESSED_FILE_PATH)

def ingest():
    files = newFilestoPush()
    for file in files:
        pushtoAsl(NEW_FILE_PATH+file)
    moveToupdated()