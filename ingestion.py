from ingestionLib import source, shovel
import logging

if __name__ == "__main__":
    logging.basicConfig(filename='output/log/shovel.log', level=logging.INFO, format='%(asctime)s %(message)s')
    source.ingest()
    #shovel.ingest()