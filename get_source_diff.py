from sourceMisses.getMisses import getMissingData
import logging
import sys

def main():
    logging.basicConfig(filename='output/log/source_miss.log', level=logging.INFO,format='%(asctime)s %(message)s')
    getMissingData()


if __name__ == "__main__":
    main()
    sys.exit()


