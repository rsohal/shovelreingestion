from shovelMisses.payments import generateShovelDataPayments
from shovelMisses.invoice import generateShovelDataforInvoice
from shovelMisses.application import generateShovelDataApplication
import logging
import sys

BU = ["mp"]

def main(start,end,type="all"):
    logging.basicConfig(filename='output/log/shovel.log', level=logging.INFO,format='%(asctime)s %(message)s')
    if type == "invoice":
        for bu in BU:
            generateShovelDataforInvoice(bu,start,end)
    elif type == "payments":
        generateShovelDataPayments(start,end)
    elif type=="application":
        generateShovelDataApplication()
    else:
        for bu in BU:
            generateShovelDataforInvoice(bu, start, end)
        generateShovelDataPayments(start,end)
        generateShovelDataApplication(start,end)

def getSysArgv():
    arg_names = ['command', 'start', 'end', 'type']
    return dict(zip(arg_names, sys.argv))

if __name__ == "__main__":

    ###########################################################################################
    ### type  = [invoice,application,payments], default => all                              ###
    ### start = Start Date , >= condition , default None, which results to 3 days back      ###
    ### end = END Date , < condition , default None, which results to today                 ###
    ###########################################################################################

    args = getSysArgv()
    print args
    if "type" in args.keys():
        if "start" in args.keys() and "end" in args.keys():
            main(args['start'],args['end'],args['type'])
        else:
            main(None,None,args['type'])
    else:
        if "start" in args.keys() and "end" in args.keys():
            main(args['start'],args['end'])
        else:
            main(None,None)




