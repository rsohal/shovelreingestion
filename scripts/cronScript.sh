#!/usr/bin/env bash
while true
do
    python get_shovel_diff.py
    python get_source_diff.py
    python ingestion.py
    sleep 86400
done