from datetime import date, timedelta
import json
import re



def convertCase(name):
    s1 = re.sub('(.)([A-Z][a-z]+)', r'\1_\2', name)
    return re.sub('([a-z0-9])([A-Z])', r'\1_\2', s1).lower()


def writeResult(path,result):
    print "No of lines in result is",len(result)
    with open(path, 'w') as f:
        for item in result:
            f.write("%s\n" % item)
    f.close()

def getDatefromDelta(t):
    return (date.today() - timedelta(t)).strftime('%s')

def getPartitonKey(start,end):
    keys = []
    for t in range(end+1,start+1):
        keys.append((date.today() - timedelta(t)).strftime('%Y%m%d'))
    return keys

def getQuery(bu,source,event):
    queries = json.loads(open('config/query.json').read())
    return queries[bu][source][event]['query']

def connectionName(bu,source,event):
    queries = json.loads(open('config/query.json').read())
    return queries[bu][source][event]['connection_name']


def getEvents(bu):
    queries = json.loads(open('config/query.json').read())
    return queries[bu]['scm'].keys()

def getBus():
    queries = json.loads(open('config/query.json').read())
    return queries.keys()

def getSCMIngestPayload(result,bu,event):
    print len(result),bu,event
    rows = []
    if (bu == "b2c"):
        if(event == "packet_dispatched"):
            for r in result:
                rows.append("""{"event":"packet_dispatched","entity_id":""" + '"' + str(r[0]) + '"' + ""","storeId":""" + '"' + str(r[2]) + '"' + ""","event_date":""" + '"' + str(r[1]) + '"' + ""","ownerId":""" + '"' + str(r[4]) + '"' + """, "storePartnerId":""" + '"' + str(r[3]) + '"' + """}""")
        if (event == "restock"):
            for r in result:
                rows.append("""{"event":"restock","entity_id":""" + '"' + str(r[1]) + '"' + ""","sku_id":""" + '"' + str(r[3]) + '"' + ""","return_id":""" + '"' + str(r[4]) + '"' + """, "order_release_id":""" + '"' + str(r[2]) + '"' + """, "event_date":""" + '"' + str(r[0]) + '"' + """, "return_type":""" + '"' + str(r[5]) + '"' + """, "storePartnerId":""" + '"' + str(r[6]) + '"' + ""","ownerId":""" + '"' + str(r[7]) + '"' + """}""")
    if (bu == "b2b"):
        for r in result:
            rows.append("""{"event":"b2b_invoice_aggregation","entity_id":""" + '"' + str(r[0]) + '"' + ""","event_date":""" + '"' + str(r[0]) + '"' + ""","source":"OMS"}""")
    if (bu=="mp"):
        if(event=="exchange"):
            for r in result:
                rows.append("""{"event":"exchange","entity_id":""" + '"' + str(r[1]) + '"' + ""","storeId":""" + '"' + str(r[3]) + '"' + ""","order_release_id":""" + '"' + str(r[0]) + '"' + ""","orderId":""" + '"' + str(r[2]) + '"' + ""","storePartnerId":""" + '"' + str(r[4]) + '"' + ""","ownerId":""" + '"' + str(r[5]) + '"' + ""","paymentPpsId":""" + '"' + str(r[1]) + '"' + ""","source":"OMS"}""")
        if (event == "packet_dispatched"):
            for r in result:
                rows.append("""{"event":"packet_dispatched","entity_id":""" + '"' + str(r[0]) + '"' + ""","storeId":""" + '"' + str(r[2]) + '"' + ""","event_date":""" + '"' + str(r[1]) + '"' + ""","ownerId":""" + '"' + str(r[4]) + '"' + """, "storePartnerId":""" + '"' + str(r[3]) + '"' + """}""")
        if (event == "packet_delivered"):
            for r in result:
                rows.append("""{"event":"packet_delivered","entity_id":""" + '"' + str(r[0]) + '"' + ""","storeId":""" + '"' + str(r[2]) + '"' + ""","event_date":""" + '"' + str(r[1]) + '"' + ""","ownerId":""" + '"' + str(r[4]) + '"' + """, "storePartnerId":""" + '"' + str(r[3]) + '"' + """}""")
        if (event == "return_refund"):
            for r in result:
                rows.append("""{"event":"return_refund","storeId":""" + '"' + str(r[2]) + '"' + ""","order_release_id":""" + '"' + str(r[1]) + '"' + ""","event_date":""" + '"' + str(r[0]) + '"' + ""","entity_id":""" + '"' + str(r[3]) + '"' + ""","ownerId":""" + '"' + str(r[5]) + '"' + """, "storePartnerId":""" + '"' + str(r[4]) + '"' + ""","paymentPpsId":""" + '"' + str(r[3]) + '"' + """}""")
        if (event == "release_lost_rto"):
            for r in result:
                e = 'release_rto_refund'
                if r[7] != 'RTO':
                    e = 'release_lost_refund'
                rows.append("""{"event":""" + '"' + e + '"' + ""","storeId":""" + '"' + str(r[2]) + '"' + ""","order_release_id":""" + '"' + str(r[1]) + '"' + ""","event_date":""" + '"' + str(r[0]) + '"' + ""","entity_id":""" + '"' + str(r[3]) + '"' + ""","ownerId":""" + '"' + str(r[5]) + '"' + """, "storePartnerId":""" + '"' + str(r[4]) + '"' + ""","paymentPpsId":""" + '"' + str(r[3]) + '"' + """}""")
        if (event == "commission"):
            for r in result:
                rows.append("""{"event":"packet_dispatched","entity_id":""" + '"' + str(r[0]) + '"' + ""","storeId":""" + '"' + str(r[2]) + '"' + ""","event_date":""" + '"' + str(r[1]) + '"' + ""","ownerId":""" + '"' + str(r[4]) + '"' + """, "storePartnerId":""" + '"' + str(r[3]) + '"' + """}""")
    return rows