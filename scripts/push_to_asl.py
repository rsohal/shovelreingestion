# # importing the requests library
import requests
import random
import string
import sys
# import multiprocessing as mp
#
# # defining the api-endpoint
API_ENDPOINT = "http://10.85.53.220/events/MYNTRAMP/process"

# filename="Payload_For_Post_Call.txt"
#
# try:
#         filename = sys.argv[1]
# except IndexError:
#         var = "somevalue"

headers = {
   'x_restbus_group_id': random.choice(string.letters),
   'x_restbus_message_id': random.choice(string.letters),
   'x_restbus_transaction_id': random.choice(string.letters),
    'content-type': 'application/json'
}
#
# pool = mp.Pool(cores)
# jobs = []
#
# with open(filename) as file:
#     data = file.read()
# split_data = data.split('\n')
#
# for string in split_data:
#     print(string)
#     # wait
#     r = requests.post(url=API_ENDPOINT, data=string,headers=headers)
#     print(r)

import multiprocessing as mp,os

def process_wrapper(chunkStart, chunkSize,filename):
    with open(filename) as f:
        f.seek(chunkStart)
        lines = f.read(chunkSize)
        process(lines)
        # for line in lines:
            # print(line)
            # process(line)

def process(string):
    split_data = string.split('\n')
    for string in split_data:
        print(string)
        # wait
        r = requests.post(url=API_ENDPOINT, data=string,headers=headers)
        print(r)

def chunkify(fname,size=1024*1024):
    fileEnd = os.path.getsize(fname)
    with open(fname,'r') as f:
        chunkEnd = f.tell()
        while True:
            chunkStart = chunkEnd
            f.seek(size,1)
            f.readline()
            chunkEnd = f.tell()
            yield chunkStart, chunkEnd - chunkStart
            if chunkEnd > fileEnd:
                break

def pushtoAsl(filename):
    #init objects
    pool = mp.Pool(25)
    jobs = []
    print filename
    #create jobs
    for chunkStart,chunkSize in chunkify(filename):
        jobs.append( pool.apply_async(process_wrapper,(chunkStart,chunkSize,filename)) )

    #wait for all jobs to finish
    for job in jobs:
        job.get()

    #clean up
    pool.close()