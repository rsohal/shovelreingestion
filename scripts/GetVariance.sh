#!/usr/bin/env bash

OMSfile=$1
Hivefile=$2

sort $OMSfile > "Pre"$OMSfile
sort $Hivefile > "Pre"$Hivefile

mv "Pre"$OMSfile $OMSfile
mv "Pre"$Hivefile $Hivefile

comm -23 $OMSfile $Hivefile > $3


#| awk '{printf "\"%s\",\n",$1}'| tr "\"" "'"
